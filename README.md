# DjiTelloGo

## Voorbeeld
```go
package main

import (
	"djitellogo"
	"fmt"
	"time"
)

func main() {
	client := djitellogo.New()
	err := client.Connect()
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	fmt.Printf("client: %v\n", client)

	err = client.SendCommand("motoron")
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	time.Sleep(5 * time.Second)

	err = client.SendCommand("motoroff")
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	ch := make(chan string)
	<-ch
}
```