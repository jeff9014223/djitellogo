BINARY_NAME=drone_test

run:
	go run .

build:
	GOOS=darwin GOARCH=arm64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_darwin_arm64 . & \
	GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_darwin_amd64 . & \
	GOOS=linux GOARCH=arm64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_linux_arm64 . & \
	GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_linux_amd64 . & \
	GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_windows_amd64.exe . & \
	GOOS=windows GOARCH=386 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_windows_386.exe . & \
	GOOS=windows GOARCH=arm64 go build -ldflags "-s -w" -v -o bin/$(BINARY_NAME)_windows_arm64.exe .