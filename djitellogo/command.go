package djitellogo

import "fmt"

func (c *Client) SendCommand(command string) error {
	fmt.Printf("Sending command: %s\n", command)
	_, err := c.EventStream.Write([]byte(command))
	if err != nil {
		return err
	}

	return nil
}
