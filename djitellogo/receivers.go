package djitellogo

import (
	"fmt"
)

func (c *Client) ResponseReceiver() {
	fmt.Printf("Waiting for responses...\n")

	for {
		var buffer = make([]byte, 1024)
		n, _, err := c.EventStream.ReadFromUDP(buffer)
		if err != nil {
			fmt.Printf("Error: %v\n", err)
			continue
		}

		data := string(buffer[:n])
		event := Event{
			Type: "response",
			Data: data,
		}
		c.EventChan <- event
	}
}
