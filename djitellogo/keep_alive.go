package djitellogo

import (
	"time"
)

func (c *Client) KeepAlive() {
	for {
		c.SendCommand("time")
		time.Sleep(10 * time.Second)
	}
}
