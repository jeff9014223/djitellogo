package djitellogo

import "fmt"

func (c *Client) ResponseListener() {
	fmt.Printf("Listening for events...\n")

	for {
		event := <-c.EventChan

		fmt.Printf("Event: %v\n", event)
	}
}
