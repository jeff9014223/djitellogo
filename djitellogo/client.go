package djitellogo

import (
	"fmt"
	"net"
)

type Client struct {
	EventStream    *net.UDPConn
	DroneIP        string
	ControlUDPPort int
	EventChan      chan Event
	StateChan      chan State
}

type Event struct {
	Type string
	Data string
}

type State struct {
	Type string
	Data string
}

func New() *Client {
	return &Client{}
}

func (c *Client) Connect() error {
	addr := fmt.Sprintf("%s:%d", DRONE_IP, CONTROL_UDP_PORT)
	conn, err := net.Dial("udp", addr)
	if err != nil {
		return err
	}

	c.DroneIP = DRONE_IP
	c.ControlUDPPort = CONTROL_UDP_PORT
	c.EventStream = conn.(*net.UDPConn)

	c.EventStream.Write([]byte("command"))

	go c.ResponseReceiver()
	go c.ResponseListener()
	go c.KeepAlive()

	return nil
}

func (c *Client) Close() error {
	return c.EventStream.Close()
}
